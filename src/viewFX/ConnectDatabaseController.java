package viewFX;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.LoadLibrariesFromFolderImpl;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * 
 * @author ChristopherRoverelli
 *
 */
public class ConnectDatabaseController extends AnchorPane implements Initializable {

    // elenco controlli grafici
    @FXML
    private Button btnOKConnectDatabase;

    @FXML
    private Button btnSearchFileDB;

    @FXML
    private Button btnSearchFileJAR;

    @FXML
    private Button btnLoad;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnLoadJAR;

    @FXML
    private Button btnBack;

    @FXML
    private Label lblConnectDatabase;

    @FXML
    private Label lblDescDB;

    @FXML
    private Label lblNameDB;

    @FXML
    private Label lblUser;

    @FXML
    private Label lblPassword;

    @FXML
    private Label lblPathJAR;

    @FXML
    private Label lblDBType;

    @FXML
    private RadioButton rbtLocalhost;

    @FXML
    private RadioButton rbtSelectFile;

    @FXML
    private RadioButton rbtLoad;

    @FXML
    private TextField txfDescDB;

    @FXML
    private TextField txfPathDB;

    @FXML
    private TextField txfNameDB;

    @FXML
    private TextField txfUser;

    @FXML
    private TextField txfPathJAR;

    @FXML
    private ChoiceBox<String> chbDBType;

    @FXML
    private ChoiceBox<String> chbLoadDB;

    @FXML
    private PasswordField psfPassword;

    @FXML
    private AnchorPane pnlConnectDatabase;

    // variabile per il collegamento con l'applicazione principale
    private MainApp mainApp;

    private static final String PORT = "5432";
   

    /**
     * inizializzazione di alcuni controlli.
     * 
     * @param location url file FXML
     * @param resources ResourceBundle
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        this.txfNameDB.setText("postgres");
        this.txfUser.setText("postgres");
        this.psfPassword.setText("password");
        this.txfPathJAR.setText("");//this.mainApp.getCC().getResPath() + System.getProperty("file.separator") + "lib_folder");
        this.chbDBType.getSelectionModel().select(0);

    }

    /**
     * collegamento con applicazione principale.
     * 
     * @param mainApp applicazione lancio UI
     */
    public void setApp(final MainApp mainApp) {
        this.mainApp = mainApp;
        this.loadCHB();
        this.updateCHBTypeDB();
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(this.mainApp.getCC().interpreterText("Load_JAR_Title"));
        alert.setContentText(this.mainApp.getCC().interpreterText("Load_JAR_Message"));
        alert.showAndWait();
    }

    /**
     * selezione file Database
     */
    @FXML
    private void selectFileDB() {

         FileChooser fileChooser = new FileChooser();
        final File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
        txfPathDB.setText(file.getPath());

    }

    /**
     * selezione directory per il caricamento delle librerie
     */
    @FXML
    private void selectFileJAR() {
        final DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("JAR");
        final File selectedDirectory = chooser.showDialog(this.mainApp.getPrimaryStage());
        txfPathJAR.setText(selectedDirectory.getAbsolutePath());

    }

    /**
     * aggiornamento choicebox per la scelta del tipo di database
     */
    private void updateCHBTypeDB() {
        this.chbDBType.getItems().clear();
        final String FILE = ("/Conf/DatabaseTypes.txt");
        try {
            final List<String> listFile = this.mainApp.getCC().getListRegex(FILE, true, "");
            this.chbDBType.setItems(FXCollections.observableList(listFile));
            this.chbDBType.getSelectionModel().select(0);
        } catch (Exception e) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText(e.getMessage());
            alert.showAndWait();
        }
    }

    /**
     * connesione al database con controlli per il tipo di connessione Es. localhost
     * o percorso file
     */
    @FXML
    private void setConnection() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        if (this.rbtLocalhost.isSelected()) {
            if (this.mainApp.getCC().setDbInfo("localhost" + ":" + ConnectDatabaseController.PORT + "/",
                    this.txfNameDB.getText(), this.txfUser.getText(), this.psfPassword.getText(),
                    this.chbDBType.getSelectionModel().getSelectedItem(), this.txfDescDB.getText())) {

                alert.setContentText(this.mainApp.getCC().interpreterText("Connect_DB"));
                alert.showAndWait();
                this.mainApp.setConnectDB(true);
                this.mainApp.editorDB();
            } else {
                alert.setContentText(this.mainApp.getCC().interpreterText("Not_Connect_DB"));
                this.mainApp.setConnectDB(false);
                alert.showAndWait();
            }
        } else if (this.rbtSelectFile.isSelected()) {
            if (this.mainApp.getCC().setDbInfo(this.txfPathDB.getText(), this.txfNameDB.getText(),
                    this.txfUser.getText(), this.psfPassword.getText(), this.chbDBType.getSelectionModel().getSelectedItem(), this.txfDescDB.getText())) {
                alert.setContentText(this.mainApp.getCC().interpreterText("Connect_DB"));
                alert.showAndWait();
                this.mainApp.setConnectDB(true);
                this.mainApp.editorDB();
            } else {
                alert.setContentText(this.mainApp.getCC().interpreterText("Not_Connect_DB"));
                this.mainApp.setConnectDB(false);
            }

        }

    }

    /**
     * caricamento elenco configurazioni salvate per la connessione del database
     */
    private void loadCHB() {
        if(this.mainApp.getCC().getErr() == null) {
            this.chbLoadDB.getItems().clear();
            this.chbLoadDB.setItems(FXCollections.observableList(this.mainApp.getCC().getDatabaseNameList()));
            this.chbLoadDB.getSelectionModel().select(0); 
        }
        else {
            Alert err = new Alert(AlertType.ERROR);
            err.setTitle("Error");
            err.setHeaderText(null);
            err.setContentText(this.mainApp.getCC().getErr().getMessage()+ "cada");
            err.showAndWait();
            this.mainApp.getCC().setErr(null);
        }
        
    }

    /**
     * caricamento configurazione per la connessione del database
     */
    @FXML
    private void loadDB() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        if (this.rbtLoad.isSelected()) {
            if (!this.chbLoadDB.getItems().isEmpty()) {
                List<String> list = new ArrayList<>(
                        this.mainApp.getCC().getDatabaseConfig(this.chbLoadDB.getSelectionModel().getSelectedItem()));
                if (this.mainApp.getCC().setDbInfo(list.get(1), list.get(0), list.get(2), list.get(3), list.get(4),
                        list.get(5))) {
                    alert.setContentText(this.mainApp.getCC().interpreterText("Connect_DB"));
                    alert.showAndWait();
                    this.mainApp.setConnectDB(true);
                    this.mainApp.editorDB();
                } else {
                    alert.setContentText(this.mainApp.getCC().interpreterText("Not_Connect_DB"));
                    this.mainApp.setConnectDB(false);
                    alert.showAndWait();
                }
            } else {
                alert.setContentText(this.mainApp.getCC().interpreterText("Conf_Empty"));
                this.mainApp.setConnectDB(false);
                alert.showAndWait();
            }

        }

    }

    /**
     * caricamento librerie dalla directory selezionata
     */
    @FXML
    private void loadJAR() {
        if (!this.txfPathJAR.getText().equals("")) {
            new LoadLibrariesFromFolderImpl(this.txfPathJAR.getText());
        }
    }

    /**
     * ritorno alla UI principale(DB Editor)
     */
    @FXML
    private void back() {
        this.mainApp.editorDB();
    }

    /**
     * cancellazione della configurazione selezionata
     */
    @FXML
    private void deleteConfDB() {
        if(!this.chbLoadDB.getSelectionModel().isEmpty()) {
            this.mainApp.getCC().deleteElemCsv(this.chbLoadDB.getSelectionModel().getSelectedItem());
           
        }
        this.loadCHB();
    }
}
