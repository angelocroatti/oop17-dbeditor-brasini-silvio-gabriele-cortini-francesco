package test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;
import controller.ControlCore;
import controller.ControlCoreImpl;
import controller.LoadLibrariesFromFolderImpl;
import model.QueryType;

/**
 * 
 * @author Francesco
 *
 */
public class TestAccess {

    private final ControlCore cc = new ControlCoreImpl();
    private static final String TABLE_NAME = "COMPANY";

    /**
     * test the access database, create and destroy table in access database.
     */
    @Test
    public void testAccessDatabase() {
        loadLib();
        final LinkedList<String> list = new LinkedList<>();
        final String string = "CREATE TABLE " + TestAccess.TABLE_NAME + "(" + "ID INT PRIMARY KEY NOT NULL,"
                + " NAME TEXT NOT NULL," + " AGE INT NOT NULL," + " ADDRESS CHAR(50) );";
        list.add(TestAccess.TABLE_NAME);
        if (this.cc.getTableList().contains(TestAccess.TABLE_NAME)) {
            this.cc.executeQuery(QueryType.DROP_TABLE, list);
        }
        list.clear();
        list.add(string);
        assertFalse(this.cc.getTableList().contains(TestAccess.TABLE_NAME));
        this.cc.executeQuery(QueryType.FREE_QUERY_UPDATE, list);
        assertTrue(this.cc.getTableList().contains(TestAccess.TABLE_NAME));
        list.clear();
        list.add(TestAccess.TABLE_NAME);
        assertTrue(this.cc.getColumnsList(list).size() == 4);
        assertTrue(this.cc.getColumnsList(list).contains("ADDRESS"));
        list.clear();
        if (this.cc.getTableList().contains(TestAccess.TABLE_NAME)) {
            list.add(TestAccess.TABLE_NAME);
            this.cc.executeQuery(QueryType.DROP_TABLE, list);
        } else {
            fail();
        }
        assertFalse(this.cc.getTableList().contains(TestAccess.TABLE_NAME));
    }

    private void loadLib() {
        // TODO Auto-generated method stub
        new LoadLibrariesFromFolderImpl(cc.getResPath() + System.getProperty("file.separator") + "lib_folder");
        cc.setDbInfo(cc.getResPath() + System.getProperty("file.separator") + "provaAccess.accdb", "", "", "",
                "ucanaccess", "access");
    }

}
