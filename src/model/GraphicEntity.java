package model;
import java.util.LinkedList;

import com.mxgraph.view.mxGraph;

/**
 * 
 * @author Luigi
 *
 */
public class GraphicEntity implements IGraphicEntity {

    private Integer id;
    private String name;
    private LinkedList<String> attributes;
    private String idAtt;

    /**
     * Constructor of GraphicEntity.
     * @param name
     * @param id
     */
    public GraphicEntity(String name, Integer id) {
        this.name =name;
        this.id = id;
        this.attributes = new LinkedList<>();
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public LinkedList<String> getAttributes() {
        return attributes;
    }

    @Override
    public void setAttributes(LinkedList<String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public void addAttributes(String attribute) {
        this.attributes.add(attribute);
    }

    @Override
    public void addAttributes(LinkedList<String> attributes) {
        this.attributes.addAll(attributes);
    }

    @Override
    public Object figure(mxGraph graph) {
        Object parent = graph.getDefaultParent();
        return (Object) graph.insertVertex(parent, this.id.toString(), this.name, 30, 40, 100, 80);
    }

    @Override
    public String getIdAtt() {
        return idAtt;
    }

    @Override
    public void setIdAtt(String idAtt) {
        this.idAtt = idAtt;
    }
}
