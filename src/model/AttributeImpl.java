/**
 * 
 */
package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author ChristopherRoverelli
 *
 */
public class AttributeImpl implements Attribute {

    private StringProperty name;
    private BooleanProperty primaryKey;
    private StringProperty type;
    private IntegerProperty lenght ;
    private BooleanProperty notNull ;
    
    public AttributeImpl(String name,Boolean pKey,String type,Integer lenght,Boolean nnKey) {
        this.name = new SimpleStringProperty(name);
        this.primaryKey= new SimpleBooleanProperty(pKey);
        this.type= new SimpleStringProperty(type);
        this.lenght= new SimpleIntegerProperty(lenght);
        this.notNull= new SimpleBooleanProperty(nnKey);
//>>>>>>> 1d7969d22b2e1a18bfab07b2eed6b45f5fac2a01
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#getName()
     */
    @Override
    public String getName() {
        return this.name.get();
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name.set(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#getPrimaryKey()
     */
    @Override
    public Boolean getPrimaryKey() {
        return this.primaryKey.get();
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#setPrimaryKey(java.lang.Boolean)
     */
    @Override
    public void setPrimaryKey(Boolean key) {
        this.primaryKey.set(key);
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#getType()
     */
    @Override
    public String getType() {
        return this.type.get();
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#setType(java.lang.String)
     */
    @Override
    public void setType(String type) {
        this.type.set(type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#getLenght()
     */
    @Override
    public Integer getLenght() {
        return this.lenght.get();
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#setLenght(int)
     */
    @Override
    public void setLenght(int lenght) {
        this.lenght.set(lenght);
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#getNot_Null()
     */
    @Override
    public Boolean getNotNull() {
        return this.notNull.get();
    }

    /*
     * (non-Javadoc)
     * 
     * @see model.Attribute#setNot_Null(java.lang.Boolean)
     */
    @Override
    public void setNotNull(Boolean key) {
        this.notNull.set(key);
    }

    @Override
    public StringProperty nameProperty() {
        return this.name;
    }

    @Override
    public BooleanProperty primaryKeyProperty() {
        return this.primaryKey;
    }

    @Override
    public StringProperty typeProperty() {
        return this.type;
    }

    @Override
    public IntegerProperty lenghtProperty() {
        return this.lenght;
    }

    @Override
    public BooleanProperty notNullProperty() {
        return this.notNull;
    }

}
