package model;

import java.util.LinkedList;

import com.mxgraph.view.mxGraph;

/**
 * interface for create GraphicEntity object.
 * @author Luigi
 *
 */
public interface IGraphicEntity {

    /**
     * method to return the id of the GraphicEntity Object
     * 
     * @return id id of the object
     */
    public Integer getId();

    /**
     * method to set the id of the GraphicEntity Object
     * 
     * @param id
     *            id of the object
     */
    public void setId(Integer id);

    /**
     * method to return the name of the GraphicEntity Object
     * 
     * @return name name of the object
     */
    public String getName();

    /**
     * method to set the name of the GraphicEntity Object
     * 
     * @param name
     *            name of the object
     */
    public void setName(String name);

    /**
     * method to return the list of Attributes's name of the GraphicEntity Object
     * 
     * @return Attributes LinkedList<String> Attributes's name
     */
    public LinkedList<String> getAttributes();

    /**
     * method to set the list of Attributes's name of the GraphicEntity Object
     * 
     * @param Attributes
     *            LinkedList<String> Attributes's name
     */
    public void setAttributes(LinkedList<String> attributes);

    /**
     * method to add a name in the list of Attributes's name of the GraphicEntity
     * Object
     * 
     * @param attribute
     *            Attribute's name
     */
    public void addAttributes(String attributes);

    /**
     * method to add names in the list of Attributes's name of the GraphicEntity
     * Object
     * 
     * @param Attributes
     *            LinkedList<String> Attributes's name
     */
    public void addAttributes(LinkedList<String> attributes);

    /**
     * method to create and return graphic representation of GraphicEntity Object
     * 
     * @param graph
     *            LinkedList<String> Attributes's name
     * @return Object Object view
     */
    public Object figure(mxGraph graph);

    /**
     * method to return the name of id of GraphicEntity Object
     * 
     * @return idAtt name of id of graphicEntity Object
     */
    public String getIdAtt();

    /**
     * method to set the name of id of GraphicEntity Object
     * 
     * @param idAtt
     *            name of id of graphicEntity Object
     */
    public void setIdAtt(String idAtt);
}
