package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import view.TableResultSet;

/**
 * Implement Database interface.
 * 
 * @author silviobrasini
 *
 */
public class DatabaseImpl implements Database {

    private final String pathDB;
    private final String nameDB;
    private final String user;
    private final String password;
    private final String typeDB;
    private final String description;

    /**
     * Constructor of DatabaseImpl.
     * 
     * @param user username
     * @param password connection password
     * @param nameDB database name
     * @param pathDB database path
     * @param typeDB connection type
     * @param description description of the connection
     */
    public DatabaseImpl(final String pathDB, final String nameDB, final String user, final String password, final String typeDB, final String description) {
        this.password = password;
        this.user = user;
        this.nameDB = nameDB;
        this.pathDB = pathDB;
        this.typeDB = typeDB;
        this.description = description;
    }

    @Override
    public String getPathDB() {
        return this.pathDB;
    }

    @Override
    public String getNameDB() {
        return this.nameDB;
    }

    @Override
    public String getUser() {
        return this.user;
    }

    @Override
    public String getPassw() {
        return this.password;
    }

    @Override
    public String getTypeDB() {
        return this.typeDB;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Boolean tryConnection() {
        try {
            final Connection c = DriverManager.getConnection("jdbc:" + this.typeDB + "://" + this.pathDB + this.nameDB, this.user, this.password);
            c.close();
        } catch (SQLException e) {
            return false;
        }
        return true;
    }

    @Override
    public String executeQuery(final QueryType queryType, final String query) {
        String error = null;
        try {
            final Connection c = DriverManager.getConnection("jdbc:" + this.typeDB + "://" + this.pathDB + this.nameDB, this.user, this.password);
            final PreparedStatement statement = c.prepareStatement(query);
            assert c != null;

            if (queryType.equals(QueryType.INTERROGATION_QUERY) || queryType.equals(QueryType.FREE_QUERY_INTERROGATION)) {
                final ResultSet rs = statement.executeQuery();
                new TableResultSet(rs);
                rs.close();
            } else {
                statement.executeUpdate();
            }

            statement.close();
            c.close();
            return error;
        } catch (SQLException e) {
            error = e.getMessage();
            return error;
        }
    }

    @Override
    public List<String> executeQueryListReturn(final QueryType queryType, final String query) {
        final List<String> list = new LinkedList<>();
        try (Connection c = DriverManager.getConnection("jdbc:" + this.typeDB + "://" + this.pathDB + this.nameDB, this.user, this.password);) {
            try (PreparedStatement statement = c.prepareStatement(query);) {
                assert c != null;
                final ResultSet rs = statement.executeQuery();
                while (rs.next()) {
                    list.add(rs.getString(1));
                }
                rs.close();
                statement.close();
                c.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
}
