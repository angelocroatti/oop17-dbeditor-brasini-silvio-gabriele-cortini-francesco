package model;

/**
 * Query made available in the program.
 * 
 * @author Francesco
 *
 */
public enum QueryType {

    /**
     * create new database.
     */
    CREATE_DATABASE,
    /**
     * modify/alter database.
     */
    ALTER_DATABASE,
    /**
     * delete/drop database.
     */
    DROP_DATABASE,
    /**
     * create new table.
     */
    CREATE_TABLE,
    /**
     * modify/alter table.
     */
    ALTER_TABLE,
    /**
     * delete/drop table.
     */
    DROP_TABLE,
    /**
     * insert new tuple.
     */
    INSERT_TUPLE,
    /**
     * modify/update tuple.
     */
    UPDATE_TUPLE,
    /**
     * delete tuple.
     */
    DELETE_TUPLE,
    /**
     * create relationship links, between 2 tables.
     */
    CREATE_FOREIGN_KEY_CONSTRAINT,
    /**
     * drop relationship links, between 2 tables.
     */
    DROP_FOREIGN_KEY_CONSTRAINT,
    /**
     * interrogation query formatted with: SELECT - FROM - WHERE - (ORDER BY / GROUP
     * BY).
     */
    INTERROGATION_QUERY,
    /**
     * free type of query, fully written by user,
     * want a return.
     */
    FREE_QUERY_INTERROGATION,
    /**
     * free type of query, fully written by user,
     * with no return.
     */
    FREE_QUERY_UPDATE,
    /**
     * list of all database.
     */
    GET_DATABASE_LIST,
    /**
     * list of all table in a database.
     */
    GET_TABLE_LIST,
    /**
     * list of all column in a table.
     */
    GET_COLUMN_LIST,
    /**
     * list of all tuple in a table.
     */
    GET_TUPLE_LIST
}
