package model;

import java.util.LinkedList;
import java.util.List;

import com.mxgraph.view.mxGraph;

/**
 * interface for create GraphicRelation object.
 * 
 * @author Luigi
 *
 */
public interface IGraphicRelation {
    /**
     * method to return the id of the GraphicRelation Object.
     * 
     * @return id id of the object
     */
    Integer getID();

    /**
     * method to set the id of the GraphicRelation Object.
     * 
     * @param iD id of the object
     */
    void setID(Integer iD);

    /**
     * method to return the name of the GraphicRelation Object.
     * 
     * @return name name of the object
     */
    String getName();

    /**
     * method to set the name of the GraphicRelation Object.
     * 
     * @param name
     *            name of the object
     */
    void setName(String name);

    /**
     * method to return the list of Attributes's name of the GraphicRelation Object.
     * 
     * @return attributes LinkedList<String> Attributes's name
     */
    List<String> getAttributes();

    /**
     * method to set the list of Attributes's name of the GraphicRelation Object.
     * 
     * @param attributes LinkedList<String> Attributes's name
     */
    void setAttributes(List<String> attributes);

    /**
     * method to add a id at list of GraphiEntity's id.
     * 
     * @param entity
     *            id of graficEntity
     */
    void addConnectEntityId(String entity);

    /**
     * method to set the list of GraphiEntity's id.
     * 
     * @param connectEntityId
     *            list of id of graficEntity
     */
    void setRightConnectEntityId(List<String> connectEntityId);

    /**
     * method to return the list of GraphiEntity's id in the GraphicRelation.
     * 
     * @param connectEntityId
     *            list of id of graficEntity
     */
    List<String> getConnectEntityId();

    /**
     * method to add a name in the list of Attributes's name of the GraphicRelation
     * Object.
     * 
     * @param attributes Attribute's name
     */
    void addAttributes(String attributes);

    /**
     * method to add names in the list of Attributes's name of the GraphicRelation
     * Object.
     * 
     * @param attributes
     *            LinkedList<String> Attributes's name
     */
    void addAttributes(LinkedList<String> attributes);

    /**
     * method to create and return graphic representation of GraphicRelation Object.
     * 
     * @param graph
     *            reference of view
     * @return Object Object view
     */
    Object figure(mxGraph graph);
}