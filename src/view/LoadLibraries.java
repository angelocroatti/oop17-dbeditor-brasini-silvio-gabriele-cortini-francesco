package view;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import controller.LoadLibrariesFromFolderImpl;

/**
 * Window for choose a folder with libraries and load them.
 * 
 * @author silviobrasini
 *
 */
public class LoadLibraries {

    private final JFrame frame;
    private final JPanel panel;
    private final JTextField jtf;
    private final JButton btnLoad;
    private final JButton btnOpen;

    /**
     * Constructor of LoadLibraries.
     * 
     * @param cc
     *            ControlCore
     */
    public LoadLibraries(final ControlCore cc) {
        this.frame = new JFrame();
        this.frame.setTitle(cc.interpreterText("LoadLibraries_Title"));
        cc.setGuiSize(this.frame, 1);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.panel = new JPanel(new GridLayout(0, 1));
        this.jtf = new JTextField();
        this.btnLoad = new JButton(cc.interpreterText("LoadLibraries_Load"));
        this.btnLoad.addActionListener(e -> {
            if (!this.jtf.getText().equals("")) {
                new LoadLibrariesFromFolderImpl(this.jtf.getText());
                this.frame.dispose();
            }
        });
        this.btnOpen = new JButton(cc.interpreterText("LoadLibraries_Open"));
        this.btnOpen.addActionListener(new OpenFileChooser(this.jtf));
        this.panel.add(this.jtf);
        this.panel.add(this.btnOpen);
        this.panel.add(this.btnLoad);
        this.frame.getContentPane().add(this.panel);
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
    }

    private class OpenFileChooser implements ActionListener {

        private final JTextField jtf;

        OpenFileChooser(final JTextField jtf) {
            this.jtf = jtf;
        }

        public void actionPerformed(final ActionEvent e) {
            final JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            final int returnVal = fileChooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                this.jtf.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }
}
