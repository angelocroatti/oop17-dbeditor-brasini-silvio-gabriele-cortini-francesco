package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import model.QueryType;

/**
 * Window for delete a tuple inside a table.
 * 
 * @author silviobrasini
 *
 */
public class DeleteTuple {

    private final JComboBox<String> jcbTable;
    private final JFrame frame;
    private final JPanel panel;
    private final JPanel panelBody;
    private final JPanel panelBtn;
    private final JLabel labelChooseTable;
    private final JLabel labelAttributeName;
    private final JLabel labelAttributeValue;
    private final JComboBox<String> jcbAttName;
    private final JTextField jtfAttValue;
    private final JButton btnDelele;

    /**
     * Constructor of deleteTuple.
     * 
     * @param cc ControlCore
     */
    public DeleteTuple(final ControlCore cc) {
        this.frame = new JFrame();
        cc.setGuiSize(this.frame, 2);
        this.frame.setTitle(cc.interpreterText("DeleteTuple_Title"));
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.panel = new JPanel(new BorderLayout());
        this.labelChooseTable = new JLabel(cc.interpreterText("DeleteTuple_Label_Choose"));
        this.jcbTable = new JComboBox<>();
        this.loadComboBox(cc, true);
        this.jcbTable.addItemListener(e -> this.loadComboBox(cc, false));
        this.labelAttributeName = new JLabel(cc.interpreterText("DeleteTuple_Label_AttName"));
        this.jcbAttName = new JComboBox<>();
        this.loadComboBox(cc, false);
        this.labelAttributeValue = new JLabel(cc.interpreterText("DeleteTuple_Label_AttValue"));
        this.jtfAttValue = new JTextField();
        this.panelBtn = new JPanel(new FlowLayout(FlowLayout.CENTER));
        this.btnDelele = new JButton(cc.interpreterText("DeleteTuple_Btn"));
        this.btnDelele.addActionListener(e -> {
            if (!this.jtfAttValue.getText().equals("")) {
                final List<String> list = new ArrayList<>();
                list.add((String) this.jcbTable.getSelectedItem());
                list.add((String) this.jcbAttName.getSelectedItem());
                list.add(this.jtfAttValue.getText());
                cc.executeQuery(QueryType.DELETE_TUPLE, list);
            } else {
                JOptionPane.showMessageDialog(null, cc.interpreterText("Empty_Message"));
            }
        });
        this.panelBody = new JPanel(new GridLayout(0, 2));
        this.panelBody.add(this.labelChooseTable);
        this.panelBody.add(this.jcbTable);
        this.panelBody.add(this.labelAttributeName);
        this.panelBody.add(this.jcbAttName);
        this.panelBody.add(this.labelAttributeValue);
        this.panelBody.add(this.jtfAttValue);
        this.panelBtn.add(this.btnDelele);
        this.panel.add(this.panelBody, BorderLayout.CENTER);
        this.panel.add(this.panelBtn, BorderLayout.SOUTH);

        frame.getContentPane().add(this.panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Load the content of a JComboBox.
     * 
     * @param cc
     * @param bool
     */
    private void loadComboBox(final ControlCore cc, final Boolean bool) {
        if (bool) {
            this.jcbTable.removeAllItems();
            if (!cc.getTableList().isEmpty()) {
                cc.getTableList().forEach(this.jcbTable::addItem);
                this.jcbTable.setSelectedIndex(0);
            }
        } else {
            this.jcbAttName.removeAllItems();
            final List<String> list = new ArrayList<>();
            list.add((String) this.jcbTable.getSelectedItem());
            if (!cc.getColumnsList(list).isEmpty()) {
                cc.getColumnsList(list).forEach(this.jcbAttName::addItem);
                this.jcbAttName.setSelectedIndex(0);
            }
        }
    }
}
