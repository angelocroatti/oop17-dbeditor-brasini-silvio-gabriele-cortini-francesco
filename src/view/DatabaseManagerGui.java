package view;

import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.ControlCore;
import model.QueryType;

/**
 * gui for create, modify and destroy database.
 * 
 * @author Francesco
 *
 */
public class DatabaseManagerGui {

    private final JFrame frame = new JFrame();
    // crea database; CREATE database
    private final JButton dbCreate = new JButton();
    // modifica database; ALTER database
    private final JButton dbAlter = new JButton();
    // distrugge database; DROP database
    private final JButton dbDrop = new JButton();

    private final JComboBox<String> dbListCombo = new JComboBox<>();
    private final JTextField databaseNewName = new JTextField();
    private final List<String> dbNameList = new LinkedList<>();
    private final List<String> queryList = new LinkedList<>();

    /**
     * gui creation.
     * 
     * @param cc
     *            ControlCore
     */
    public DatabaseManagerGui(final ControlCore cc) {
        cc.setGuiSize(this.frame, 1);
        final JPanel pane = new JPanel(new GridLayout(3, 2));
        this.resetComboBox(cc);
        this.databaseNewName.setText(cc.interpreterText("New_Database_Name"));
        this.dbCreate.setText(cc.interpreterText("CREATE_NEW_DATABASE"));
        this.dbAlter.setText(cc.interpreterText("ALTER_DATABASE_NAME"));
        this.dbDrop.setText(cc.interpreterText("DROP_DATABASE"));

        this.dbCreate.addActionListener(e -> {
            final String error;
            this.queryList.add(this.databaseNewName.getText());
            error = cc.executeQuery(QueryType.CREATE_DATABASE, this.queryList);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
            this.resetComboBox(cc);
        });

        this.dbAlter.addActionListener(e -> {
            final String error;
            this.queryList.add(this.dbNameList.get(this.dbListCombo.getSelectedIndex()));
            this.queryList.add(this.databaseNewName.getText());
            error = cc.executeQuery(QueryType.ALTER_DATABASE, this.queryList);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
            this.resetComboBox(cc);
        });

        this.dbDrop.addActionListener(e -> {
            final String error;
            this.queryList.add(this.dbNameList.get(this.dbListCombo.getSelectedIndex()));
            error = cc.executeQuery(QueryType.DROP_DATABASE, this.queryList);
            if (error != null) {
                JOptionPane.showMessageDialog(null, error);
            }
            this.resetComboBox(cc);
        });

        pane.add(this.dbListCombo);
        pane.add(this.databaseNewName);
        pane.add(this.dbCreate);
        pane.add(this.dbAlter);
        pane.add(this.dbDrop);
        this.frame.setContentPane(pane);
        this.frame.setTitle(cc.interpreterText("Database_Manager"));
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.frame.setVisible(true);
    }

    private void resetComboBox(final ControlCore cc) {
        // TODO Auto-generated method stub
        this.dbListCombo.removeAllItems();
        this.dbNameList.clear();
        this.queryList.clear();
        //this.dbNameList = cc.getDatabaseList();
        cc.getDatabaseList().forEach(this.dbNameList::add);
        if (!this.dbNameList.isEmpty()) {
            this.dbNameList.forEach(this.dbListCombo::addItem);
            this.dbListCombo.setSelectedIndex(0);
            this.dbAlter.setEnabled(true);
            this.dbDrop.setEnabled(true);
        } else {
            this.dbAlter.setEnabled(false);
            this.dbDrop.setEnabled(false);
        }
        // this.dbListCombo.setEditable(true);
    }
}
