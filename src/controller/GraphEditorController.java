package controller;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
import model.GraphicAttributes;
import model.GraphicEntity;
import model.GraphicID;
import model.GraphicRelation;
import model.IGraphicAttribute;
import model.IGraphicEntity;
import model.IGraphicID;
import model.IGraphicRelation;;

/**
 * 
 * @author Luigi
 *
 */
public class GraphEditorController implements IGraphEditorController {

    List<IGraphicEntity> gel;
    List<IGraphicRelation> grl;
    List<IGraphicAttribute> gal;
    List<IGraphicID> gil;
    String name;
    Integer incId;

    /**
     * Constructor of GraphEditorController.
     */
    public GraphEditorController() {
        gel = new LinkedList<>();
        grl = new LinkedList<>();
        gal = new LinkedList<>();
        gil = new LinkedList<>();
        name = "";
        incId = 0;
    }

    @Override
    public void addEntity(mxGraph graph, String name) {
        IGraphicEntity entity = new GraphicEntity(name, incId);

        graph.getModel().beginUpdate();
        entity.figure(graph);
        graph.getModel().endUpdate();
        incId++;
        gel.add(entity);
    }

    @Override
    public void addRelation(mxGraph graph, String name) {
        IGraphicRelation relation = new GraphicRelation(incId, name);

        graph.getModel().beginUpdate();
        relation.figure(graph);
        graph.getModel().endUpdate();
        incId++;
        grl.add(relation);

    }

    @Override
    public void addAttribute(mxGraph graph, String name) {

        IGraphicAttribute a = new GraphicAttributes(incId.toString(), name);

        graph.getModel().beginUpdate();
        a.figreAtt(graph);
        graph.getModel().endUpdate();
        incId++;
        gal.add(a);
    }

    @Override
    public void addId(mxGraph graph, String name) {

        IGraphicID a = new GraphicID(incId.toString(), name);

        graph.getModel().beginUpdate();
        a.figreAtt(graph);
        graph.getModel().endUpdate();
        incId++;
        gil.add(a);
    }

    @Override
    public boolean connect(String target, String source) {
        IGraphicEntity entT = gel.stream().filter(x -> x.getName() == target).collect(Collectors.toList()).isEmpty()
                ? null
                : gel.stream().filter(x -> x.getName() == target).collect(Collectors.toList()).get(0);
        IGraphicRelation relT = grl.stream().filter(x -> x.getName() == target).collect(Collectors.toList()).isEmpty()
                ? null
                : grl.stream().filter(x -> x.getName() == target).collect(Collectors.toList()).get(0);
        IGraphicAttribute attrS = gal.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).isEmpty()
                ? null
                : gal.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).get(0);
        IGraphicID idS = gil.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).isEmpty() ? null
                : gil.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).get(0);
        IGraphicEntity entS = gel.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).isEmpty()
                ? null
                : gel.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).get(0);
        IGraphicRelation relS = grl.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).isEmpty()
                ? null
                : grl.stream().filter(x -> x.getName() == source).collect(Collectors.toList()).get(0);

        if (entT != null) {
            if (attrS != null) {
                entT.addAttributes(attrS.getName());
            } else if (idS != null) {
                if (entT.getIdAtt() == null) {
                    entT.setIdAtt(idS.getName());
                } else if (relS != null) {
                    if (entT.getIdAtt() != null)
                        relS.getConnectEntityId().add(entT.getIdAtt());
                    else {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }

        if (relT != null) {
            if (attrS != null) {
                relT.addAttributes(attrS.getName());
            } else if (entS != null) {
                if (entS.getIdAtt() != null) {
                    relT.getConnectEntityId().add(entT.getIdAtt());
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public void cancelElement(Object o, mxGraph graph) {
        graph.getModel().remove(o);
        mxCell etr = (mxCell) o;
        IGraphicEntity ent = gel.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                .collect(Collectors.toList()).isEmpty() ? null
                        : gel.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                                .collect(Collectors.toList()).get(0);
        IGraphicRelation rel = grl.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                .collect(Collectors.toList()).isEmpty() ? null
                        : grl.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                                .collect(Collectors.toList()).get(0);
        IGraphicAttribute attr = gal.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                .collect(Collectors.toList()).isEmpty() ? null
                        : gal.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                                .collect(Collectors.toList()).get(0);
        IGraphicID id = gil.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                .collect(Collectors.toList()).isEmpty() ? null
                        : gil.stream().filter(x -> x.getName().equals(etr.getValue().toString()))
                                .collect(Collectors.toList()).get(0);

        gel.remove(ent);
        grl.remove(rel);
        gal.remove(attr);
        gil.remove(id);

    }

    @Override
    public void printCode() {
        String path = new java.io.File("").getAbsolutePath() + "\\res\textpostgreSQL.txt";
        System.out.println(path);
        List<String> lis = new LinkedList<>();
        lis.add("CREATE DATABASE " + name + ";");
        String query = "CREATE DATABASE " + name + "; ";
        for (IGraphicEntity e : gel) {
            query += "CREATE TABLE " + e.getName() + "(  " + e.getIdAtt() + " INT PRIMARY KEY NOT NULL";
            lis.add("CREATE TABLE " + e.getName() + "(");
            lis.add(e.getIdAtt() + " INT PRIMARY KEY NOT NULL");
            for (String a : e.getAttributes()) {
                query += ", " + a + " CHAR(50) NOT NULL";
                lis.add(", " + a + " CHAR(50) NOT NULL");
            }
            query += " );";
            lis.add(");");
        }

        Path file = FileSystems.getDefault().getPath(path);
        try {
            Files.write(file, lis, Charset.forName("UTF-8"));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        System.out.println(query);
        gel.clear();
    }

}
